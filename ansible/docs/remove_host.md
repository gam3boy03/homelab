# How to remove a host?

1. Set host to absent in `host_vars`.
2. Run Playbook until it fails.
3. Remove host from `hosts` file.
4. Finished