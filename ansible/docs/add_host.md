# How to add a new host

1. Add a new file with the correct hostname to `host_vars`
2. Add the same to the `hosts` file. (Add it to the `containers` group)
3. Add iptables rules in `roles/iptables/tasks/nodes/{{ hostname }}.yml`. Orient on the other configs
3. The host will be preprovisioned with some sensible defaults.
4. Deployment (if not done differently) in `deploy.yml` hinzufügen.