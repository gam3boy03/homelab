# Homelab

## TLDR

This Repository contains all the code for my new (dockerized and automated) Homelab

## TODO

- [ ] Setup proxmox automated container provisioning as simple as possible (only need to add a host)

- [ ] Setup a Provisioning flow for the docker host
    - [x] Set Package repo to lrz repo
    - [x] Run updates
    - [x] Setup users & ssh keys
      - [x] ansible
      - [x] paul.koehler
    - [x] Setup admin group with sudo rights
    - [x] Setup ssh
      - [x] setup authorized keys
      - [x] ssh-key authentication only
    - [x] Set Hostname to ansible_host name
    - [x] Setup Firewall Rules (iptables)
      - [x] Full lock down
      - [x] Allow ssh from local network (for now)
    - [ ] Setup docker
    - [ ] Setup Monitoring and Logging with Grafana / Loki Cloud

- [x] Setup ansible vault

- [ ] Setup simple ansible roles for running docker-compose files

- [ ] Setup a cloudflare Tunnel for secure SSH (maybe build a bastion)
  - [ ] Remove access from local network

- [ ] Build pipeline for automated deployment

- [ ] Setup Authelia
    - [ ] Setup cloudflare tunnel
    - [ ] Setup CNAME
    - [ ] Require 2FA

- [ ] Setup Seafile with cloudflare tunnel (maybe minimized Nextcloud?)
    - [ ] Setup cloudflare tunnel
    - [ ] Setup CNAME
    - [ ] Setup Authelia as IdP

- [ ] Setup Homeassistant with cloudflare tunnel
    - [ ] Setup cloudflare tunnel
    - [ ] Setup CNAME
    - [ ] Setup Authelia as IdP
    - [ ] Setup 2FA Authentication with Middleware

- [ ] Setup Bitwarden with cloudflare tunnel (Look for other options as well)
    - [ ] Setup cloudflare tunnel
    - [ ] Setup CNAME
    - [ ] Setup Authelia as IdP